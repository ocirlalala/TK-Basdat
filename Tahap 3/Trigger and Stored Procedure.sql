a. Pada Tabel NARASUMBER terdapat kolom jumlahBerita. Kolom ini akan ter-update otomatis oleh stored procedure setiap kali ada INSERT/DELETE pada tabel NARASUMBER_BERITA menggunakan trigger.


ALTER TABLE NARASUMBER ADD COLUMN jumlahBerita INT DEFAULT 0;

CREATE OR REPLACE FUNCTION update_jumlah_berita()
RETURNS trigger AS
$$
	DECLARE
 	jumlah_beritaN INTEGER;
 	idN INTEGER;
	BEGIN
		
			SELECT N.id INTO idN
			FROM   NARASUMBER N
			WHERE  N.id = NEW.id_narasumber;
			SELECT COUNT(B.*) INTO jumlah_beritaN
			FROM BERITA B, NARASUMBER N, NARASUMBER_BERITA NB
			WHERE B.url = NB.url_berita AND NB.id_narasumber = N.id
				  AND N.id = idN;

			IF (TG_OP = 'INSERT' OR TG_OP = 'DELETE') THEN
				UPDATE NARASUMBER N SET jumlahBerita = jumlah_beritaN
				WHERE  N.id = idN;
				RETURN NEW;	
			END IF;
		
	END;
$$
LANGUAGE plpgsql;



-- yang baru (udah bener)
CREATE OR REPLACE FUNCTION update_jumlah_berita()
RETURNS trigger AS
$$
	DECLARE
 	jumlah_beritaN INTEGER;
 	idN INTEGER;
	BEGIN
		IF (TG_OP = 'INSERT') THEN
			SELECT N.id INTO idN
			FROM   NARASUMBER N
			WHERE  N.id = NEW.id_narasumber;
		ELSIF(TG_OP = 'DELETE') THEN
			SELECT N.id INTO idN
			FROM   NARASUMBER N
			WHERE  N.id = OLD.id_narasumber;
		END IF;



			SELECT COUNT(B.*) INTO jumlah_beritaN
			FROM BERITA B, NARASUMBER N, NARASUMBER_BERITA NB
			WHERE B.url = NB.url_berita AND NB.id_narasumber = N.id AND N.id = idN;

			IF (TG_OP = 'INSERT') THEN
				UPDATE NARASUMBER N SET jumlah_berita = jumlah_beritaN
				WHERE  N.id = idN;
				RETURN NEW;

			ELSIF (TG_OP = 'DELETE') THEN
				UPDATE NARASUMBER N SET jumlah_berita = jumlah_beritaN
				WHERE  N.id = idN;
				RETURN OLD;	
			END IF;
		
	END;
$$
LANGUAGE plpgsql;






CREATE TRIGGER jumlah_berita_trigger
AFTER INSERT OR DELETE
ON NARASUMBER_BERITA FOR EACH ROW
EXECUTE PROCEDURE update_jumlah_berita();

INSERT INTO NARASUMBER_BERITA VALUES ('http://redcross.org',1);
INSERT INTO NARASUMBER_BERITA VALUES ('https://vk.com',2);


SELECT COUNT(B.*) 
FROM BERITA B, NARASUMBER N, NARASUMBER_BERITA NB
WHERE B.url = NB.url_berita AND NB.id_narasumber = N.id
AND N.id = 1;






CREATE OR REPLACE FUNCTION tambah_kupon()
RETURNS trigger AS
$$
	DECLARE
 		jumlah_beritaN INTEGER;
 		idN INTEGER;
 		idK INTEGER;
	BEGIN
		SELECT jumlah_berita INTO jumlah_beritaN
		FROM   NARASUMBER N
		WHERE  N.id = NEW.id_narasumber;

		SELECT N.id INTO idN
		FROM   NARASUMBER N
		WHERE  N.id = NEW.id_narasumber;

		IF (TG_OP = 'INSERT') THEN
			SELECT COUNT(*) + 1 INTO idK
			FROM KUPON;

			IF (jumlah_beritaN % 10 = 0) THEN
				INSERT INTO KUPON VALUES (idK, localtimestamp, localtimestamp + interval '3 month', idN);	
			END IF;
			RETURN NEW;
		END IF;
	END;
$$
LANGUAGE plpgsql;


CREATE TRIGGER tambah_kupon_trigger
AFTER INSERT 
ON NARASUMBER_BERITA FOR EACH ROW
EXECUTE PROCEDURE tambah_kupon();

https://joomla.org
http://cpanel.net
http://ameblo.jp
http://liveinternet.ru 
http://hubpages.com

INSERT INTO NARASUMBER_BERITA VALUES ('https://joomla.org',1);
INSERT INTO NARASUMBER_BERITA VALUES ('http://cpanel.net',1);
INSERT INTO NARASUMBER_BERITA VALUES ('http://ameblo.jp',1);
INSERT INTO NARASUMBER_BERITA VALUES ('http://liveinternet.ru',1);
INSERT INTO NARASUMBER_BERITA VALUES ('http://hubpages.com',1);



Kurang:
1. NARASUMBER_BERITA 1
2. DOSEN 2
3. 