CREATE SCHEMA BMNC;

SET search_path to BMNC;

CREATE TABLE UNIVERSITAS (
	id INT NOT NULL,
	jalan varchar(100) NOT NULL,
	kelurahan varchar(50) NOT NULL,
	provinsi varchar(50) NOT NULL,
	kodepos varchar(10) NOT NULL,
	website varchar(50) NOT NULL,
	PRIMARY KEY (id)
);	

CREATE TABLE BERITA (
	url varchar(50) NOT NULL,
	judul varchar(100) NOT NULL,
	topik varchar(100) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	jumlah_kata INT NOT NULL,
	rerata_rating DOUBLE PRECISION NOT NULL,
	id_universitas INT NOT NULL,
	PRIMARY KEY (url),
	FOREIGN KEY (id_universitas) REFERENCES UNIVERSITAS(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE RATING (
	url_berita varchar(50) NOT NULL,
	ip_address varchar(50) NOT NULL,
	nilai DOUBLE PRECISION NOT NULL,
	PRIMARY KEY (url_berita,ip_address),
	FOREIGN KEY (url_berita) REFERENCES BERITA(url) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE RIWAYAT (
	url_berita varchar(50) NOT NULL,
	id_riwayat INT NOT NULL,
	waktu_revisi timestamp NOT NULL,
	konten text NOT NULL,
	PRIMARY KEY (url_berita,id_riwayat),
	FOREIGN KEY (url_berita) REFERENCES BERITA(url) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE TAG (
	url_berita varchar(50) NOT NULL,
	tag varchar(50) NOT NULL,
	PRIMARY KEY (url_berita,tag),
	FOREIGN KEY (url_berita) REFERENCES BERITA(url) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE NARASUMBER (
	id INT NOT NULL,
	nama varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	tempat varchar(50) NOT NULL,
	tanggal varchar(50) NOT NULL,
	no_hp varchar(50) NOT NULL,
	jumlah_berita INT NOT NULL,
	rerata_kata INT NOT NULL,
	id_universitas INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_universitas) REFERENCES UNIVERSITAS(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE NARASUMBER_BERITA (
	url_berita varchar(50) NOT NULL,
	id_narasumber INT NOT NULL,
	PRIMARY KEY (url_berita,id_narasumber),
	FOREIGN KEY (url_berita) REFERENCES BERITA(url) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_narasumber) REFERENCES NARASUMBER(id)  ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE HONOR (
	id_narasumber INT NOT NULL,
	tgl_diberikan timestamp NOT NULL,
	jumlah_berita INT NOT NULL,
	jumlah_gaji INT NOT NULL,
	PRIMARY KEY (id_narasumber,tgl_diberikan),
	FOREIGN KEY (id_narasumber) REFERENCES NARASUMBER(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE DOSEN (
	id_narasumber INT NOT NULL,
	nik_dosen varchar(20) NOT NULL,
	jurusan varchar(20) NOT NULL,
	PRIMARY KEY (id_narasumber),
	FOREIGN KEY (id_narasumber) REFERENCES NARASUMBER(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE STAF (
	id_narasumber INT NOT NULL,
	nik_staf varchar(20) NOT NULL,
	posisi varchar(20) NOT NULL,
	PRIMARY KEY (id_narasumber),
	FOREIGN KEY (id_narasumber) REFERENCES NARASUMBER(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE MAHASISWA (
	id_narasumber INT NOT NULL,
	npm varchar(20) NOT NULL,
	status varchar(20) NOT NULL,
	PRIMARY KEY (id_narasumber),
	FOREIGN KEY (id_narasumber) REFERENCES NARASUMBER(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE REKENING (
	nomor VARCHAR(20) NOT NULL,
	nama_bank VARCHAR(20) NOT NULL,
	id_narasumber INT NOT NULL,
	PRIMARY KEY (nomor),
	FOREIGN KEY (id_narasumber) REFERENCES NARASUMBER(id) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE KUPON (
	id INT NOT NULL,
	tgl_diberikan TIMESTAMP NOT NULL,
	tgl_kadaluarsa TIMESTAMP NOT NULL,
	id_narasumber INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_narasumber) REFERENCES NARASUMBER(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE KOMENTAR (
	id INT NOT NULL,
	tanggal TIMESTAMP NOT NULL,
	jam TIME NOT NULL,
	konten VARCHAR(100) NOT NULL,
	nama_user VARCHAR(50) NOT NULL,
	email_user VARCHAR(50) NOT NULL,
	url_user VARCHAR(50) NOT NULL,
	url_berita VARCHAR(50) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (url_berita) REFERENCES BERITA(url) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE POLLING (
	id INT NOT NULL,
	polling_start TIMESTAMP NOT NULL,
	polling_end TIMESTAMP NOT NULL,
	total_responden INT NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE POLLING_BERITA (
	id_polling INT NOT NULL,
	url_berita VARCHAR(50) NOT NULL,
	PRIMARY KEY (id_polling),
	FOREIGN KEY (id_polling) REFERENCES POLLING(id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (url_berita) REFERENCES BERITA(url) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE POLLING_BIASA (
	id_polling INT NOT NULL,
	url VARCHAR(50) NOT NULL,
	deskripsi VARCHAR(100) NULL,
	PRIMARY KEY (id_polling),
	FOREIGN KEY (id_polling) REFERENCES POLLING(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE RESPON (
	id_polling INT NOT NULL,
	jawaban VARCHAR(50) NOT NULL,
	jumlah_dipilih INT NOT NULL,
	PRIMARY KEY (id_polling, jawaban),
	FOREIGN KEY (id_polling) REFERENCES POLLING(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE RESPONDEN (
	id_polling INT NOT NULL,
	ip_address VARCHAR(50) NOT NULL,
	PRIMARY KEY (id_polling, ip_address),
	FOREIGN KEY (id_polling) REFERENCES POLLING(id) ON UPDATE CASCADE ON DELETE CASCADE
);

